$(document).ready(function() {

    $("#keyword").keyup( function() {
        var data = $("#keyword").val();
        var url = 'https://www.googleapis.com/books/v1/volumes?q=' + data
        $.ajax({
            url: url,
            success: function(hasil) {
                var obj_hasil = $('#hasil');
                obj_hasil.empty();
    
                for(i = 0; i < hasil.items.length; i++){
                    var title = hasil.items[i].volumeInfo.title;
                    var cover = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var author = hasil.items[i].volumeInfo.authors;
                    obj_hasil.append('<tr><td>' + title + '</td> <td><img src=' + cover + '> </td> <td>' + author + '</td></tr>')
                }
            }
        });
    });
    
});
console.log("Test")
