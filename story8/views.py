from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.
def buku(request):
    return render(request, 'books.html')

def data(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    req = requests.get(url)
    data = json.loads(req.content)
    return JsonResponse(data, safe=False)

