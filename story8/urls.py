from django.urls import path, include
from . import views

app_name = 'story8'
urlpatterns = [
    path('', views.buku, name="buku"),
    path('data/', views.data, name="data"),
]
