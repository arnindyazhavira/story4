from django.urls import path, include
from . import views

app_name = 'story1'
urlpatterns = [
    path('', views.home, name = 'home'),
    path('about', views.about, name= 'about'),
    path('education', views.education, name = 'education'),
]