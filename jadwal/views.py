from django.shortcuts import render, redirect
from .models import Sched
from .forms import Matkul

# Create your views here.
def add(request):
    if request.method == 'POST':
        form = Matkul(request.POST)
        if form.is_valid():
            sched = form.save()
            sched.save()
            return redirect('jadwal:view')
    else:
        form = Matkul()
    return render(request, 'add.html', {'form': form})
def view(request):
    jadwal = Sched.objects.order_by('Subject')
    return render(request, 'view.html', {'jadwal':jadwal})
def detail(request, id):
    detail = Sched.objects.get(id=id)
    return render(request, 'detail.html', {'detail':detail})
def delete(request, id):
    Sched.objects.get(id = id).delete()
    return redirect('jadwal:view')

