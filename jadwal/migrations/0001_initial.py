# Generated by Django 3.1.2 on 2020-11-24 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sched',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Subject', models.CharField(max_length=60)),
                ('Lecturer', models.CharField(max_length=60)),
                ('Credit', models.CharField(max_length=60)),
                ('Description', models.TextField(max_length=60)),
                ('Semester', models.CharField(max_length=60)),
                ('Room', models.CharField(default='Online Class', max_length=60)),
            ],
        ),
    ]
