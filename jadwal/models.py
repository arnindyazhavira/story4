from django.db import models

# Create your models here.
class Sched(models.Model):
    Subject = models.CharField(max_length = 60)
    Lecturer = models.CharField(max_length = 60)
    Credit = models.CharField(max_length = 60)
    Description = models.TextField(max_length = 60)
    Semester = models.CharField(max_length = 60)
    Room = models.CharField(max_length = 60, default = "Online Class")
