from django import forms

from .models import Sched

class Matkul(forms.ModelForm):
    class Meta:
        model = Sched
        fields = ['Subject', 'Lecturer', 'Credit', 'Description', 'Semester', 'Semester', 'Room']
        widgets = {
            'Subject' : forms.TextInput(attrs = {'class':'form-control'}),
            'Lecturer' : forms.TextInput(attrs = {'class':'form-control'}),
            'Credit' : forms.TextInput(attrs = {'class':'form-control'}),
            'Description' : forms.TextInput(attrs = {'class':'form-control'}),
            'Semester' : forms.TextInput(attrs = {'class':'form-control'}),
            'Room' : forms.TextInput(attrs = {'class':'form-control'}),
        }
