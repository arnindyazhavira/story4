from django.urls import path, include
from . import views

app_name = 'jadwal'
urlpatterns = [
    path('', views.add, name = 'add'),
    path('view', views.view, name = 'view'),
    path('detail/<id>', views.detail, name = 'detail'),
    path('delete/<id>', views.delete, name = 'delete'),
]