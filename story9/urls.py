from django.urls import path
from story9.views import SignUpView
from . import views

app_name = 'story9'

urlpatterns = [
    path('masuk', views.masuk, name='masuk/'),
    path('login/', views.login, name='login'),
    path('logout', views.logout, name='logout/'),
    path('signup', SignUpView.as_view(), name='signup'),
]
