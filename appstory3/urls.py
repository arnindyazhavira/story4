from django.urls import path, include
from . import views

app_name = 'appstory3'
urlpatterns = [
    path('', views.project, name = 'project'),
]